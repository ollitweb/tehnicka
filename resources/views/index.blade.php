<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tehnička škola</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Simple Line Font -->
    <link rel="stylesheet" href="css/simple-line-icons.css">
    <!-- Slider / Carousel -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- Main CSS -->
    <link href="css/style.css" rel="stylesheet">
	

<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color:   #cbb58b;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color:   #cbb58b;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
</head>

<body>
    <!--============================= HEADER =============================-->
    <header>
        <div class="container nav-menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="index.html"><img src="images/responsive-logo.png" class="responsive-logo img-fluid" alt="responsive-logo"></a>
                </div>
            </div>
            <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                    <span class="icon-menu"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <!--<ul class="navbar-nav">-->
                        <!--<li class="nav-item">-->
                            <!--<a class="nav-link" href="about.html">About<span class="sr-only">(current)</span></a>-->
                        <!--</li>-->
                        <!--<li class="nav-item">-->
                            <!--<a class="nav-link" href="admission-form.html">Admissions</a>-->
                        <!--</li>-->
                        <!--<li class="nav-item">-->
                            <!--<a class="nav-link" href="academics.html">Academics</a>-->
                        <!--</li>-->
                        <!--<li class="nav-logo">-->
                            <!--<a href="#" class="navbar-brand"><img src="images/logo.png" class="img-fluid" alt="logo"></a>-->
                        <!--</li>-->
                        <!--<li class="nav-item">-->
                            <!--<a class="nav-link" href="research.html">Research</a>-->
                        <!--</li>-->
                        <!--<li class="nav-item dropdown">-->
                            <!--<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
                                <!--Pages-->
                            <!--</a>-->
                            <!--<ul class="dropdown-menu">-->
                             <!--<li><a class="dropdown-item" href="index-2.html">Home Style Two</a></li>-->
                                    <!--<li><a class="dropdown-item" href="index-video.html">Home Video</a></li>-->
                                <!--<li><a class="dropdown-item" href="blog.html">Blog</a></li>-->
                                <!--<li><a class="dropdown-item" href="blog-post.html">Blog Post</a></li>-->
                                <!--<li><a class="dropdown-item" href="index-landing-page.html">Landing Page</a></li>-->
                                <!--<li><a class="dropdown-item" href="events.html">Events</a></li>-->
                                <!--<li><a class="dropdown-item" href="course-detail.html">Course Details</a></li>-->
                                <!--<li><a class="dropdown-item" href="campus-life.html">Campus Life</a></li>-->
                                <!--<li><a class="dropdown-item" href="our-teachers.html">Our Teachers</a></li>-->
                                <!--<li><a class="dropdown-item" href="teachers-single.html">Teachers Single</a></li>-->
                                <!--<li><a class="dropdown-item" href="gallery.html">Gallery</a></li>  -->
                                <!--<li><a class="dropdown-item" href="shortcodes.html">Shortcodes</a></li>-->
                                <!--<li class="dropdown">-->
                                  <!--<a class="dropdown-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Others Pages</a>-->
                                  <!--<ul class="dropdown-menu dropdown-menu1"> -->
                                    <!--<li><a class="dropdown-item" href="notice-board.html">Notice Board</a></li>-->
                                    <!--<li><a class="dropdown-item" href="chairman-speech.html">Chairman Speech</a></li>-->
                                    <!--<li><a class="dropdown-item" href="sample-page.html">Sample Page</a></li>-->
                                    <!--<li><a class="dropdown-item" href="faq.html">FAQ</a></li>-->
                                    <!--<li><a class="dropdown-item" href="login.html">Login</a></li>-->
                                    <!--<li><a class="dropdown-item" href="sign-up.html">Sign Up</a></li>-->
                                    <!--<li><a class="dropdown-item" href="coming-soon.html">Coming Soon</a></li> -->
                                <!--</ul>-->
                            <!--</li>-->
                        <!--</ul>-->
                    <!--</li>-->
                    <!--<li class="nav-item">-->
                        <!--<a class="nav-link" href="contact.html">Contact</a>-->
                    <!--</li>-->
                <!--</ul>-->
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Škola
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown">
                                    <a class="dropdown-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Mašinstvo i obrada metala</a>
                                    <ul class="dropdown-menu dropdown-menu1">
                                        <li><a class="dropdown-item" href="/masinski_tehnicar">Mašinski tehničar za kompujtersko upravljanje</a></li>
                                        <li><a class="dropdown-item" href="#">Automehaničar</a></li>
                                        <li><a class="dropdown-item" href="#">Bravar</a></li>
                                        <li><a class="dropdown-item" href="#">Zavarivač</a></li>
                                    </ul>
                                </li>
                                <li><a class="dropdown-item" href="/saobracaj">Saobracaj</a></li>
                                <li><a class="dropdown-item" href="/turizam">Turizam i ugostiteljstvo</a></li>
                                <li><a class="dropdown-item" href="/skolska_dokumenta">Školska dokumenta</a></li>
                                <li><a class="dropdown-item" href="#">Oglasna tabla - obavestenja</a></li>
								<li><a class="dropdown-item" href="/rec_direktora">Reč direktora</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Dom učenika
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="/vaspitni-rad">Vaspitni rad</a></li>
                                <li><a class="dropdown-item" href="/upis-u-dom">Upis u dom</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="/ucenicki-kutak" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Učenički kutak
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="/ucenicki-parlament">Učenički parlament</a></li>
                                <li><a class="dropdown-item" href="/skolske-novine-masinac">Školske novine " Mašinac"</a></li>
                                <li><a class="dropdown-item" href="/dramska-sekcija">Dramska sekcija</a></li>
                                <li><a class="dropdown-item" href="/korisni-linkovi">Korisni linkovi</a></li>
                                <li><a class="dropdown-item" href="/djaci-generacije">Đaci generacije</a></li>
                            </ul>
                        </li>
                        <li class="nav-logo">
                            <a href="/" class="navbar-brand"><img src="images/logo.png" class="img-fluid" alt="logo"></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/novosti">Novosti</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/galerija">Galerija</a>
                        </li>
                        <!--<li class="nav-item">-->
                            <!--<a class="nav-link" href="research.html">javne nabavke</a>-->
                        <!--</li>-->
                         <li class="nav-item">
                            <a class="nav-link" href="/kontakt">Kontakt</a>
                        </li>
                    </ul>
            </div>
        </nav>
    </div>
</div>
</div>

<div class="slider">
    @foreach($home_slider as $h_s)
      <img class="mySlides" src="{{ $h_s->image }}">
    @endforeach

      <button class="w3-button w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
      <button class="w3-button w3-display-right" onclick="plusDivs(1)">&#10095;</button>
</div>

</header>
<!--//END HEADER -->
<!--============================= ABOUT =============================-->
<section class="clearfix about">
    <div class="container">
			<div class="row">
				<div class="col-lg-4"> 
				<img src="images/booksandapple.png"></img>
				<br>
				<h3 style="text-align:center;">Upis</h3>
				<p style="text-align: justify; text-align-last: center;">Упис у школску 2018/2019. годину i rezultati upisa.</p>
				<br>
				<p><a href="">Nastavi</a></p>
				</div>
				<div class="col-lg-4"> 
				<img src="images/compass.png"></img>
				<br>
				<h3 style="text-align:center;">Mašinac</h3>
				<p style="text-align: justify; text-align-last: center;">Hовине Машинац, редовно обавештавају о свим битним темама школе.</p>
				<br>
				<p><a href="">Nastavi</a></p>
				</div>
				<div class="col-lg-4">
				<img src="images/closedbook.png"></img>	
				<br>
				<h3 style="text-align:center;">Novosti</h3>
				<p style="text-align: justify; text-align-last: center;">Прочитајте све новости из школе, дома, са спортских терена и околине.</p>
				<br>
				<p><a href="">Nastavi</a></p>
				</div>
			</div>
    </div>
    </section>
    <!--//END ABOUT -->
    <!--============================= OUR COURSES =============================-->
    <section style="background-color: #f2f2f2; padding-bottom: 100px;" class="tab">
	<div class="container">
       <h2 style="text-align:center; color:black; margin-top:20px;">Obaveštenja</h2>

		<div style="background-color: #b6a27d; " class="tab">
		  <button class="tablinks active" onclick="openCity(event, 'London')" style="color:white;" >Obrazovna područja</button>
		  <button class="tablinks" onclick="openCity(event, 'Paris')" style="color:white;">O školi i domu</button>
		  <button class="tablinks" onclick="openCity(event, 'Tokyo')" style="color:white;">Kontakt</button>
		  <button class="tablinks" onclick="openCity(event, 'Tokyo2')" style="color:white;">Javne nabavke</button>
		</div>

		<div style="display:block; background-color: #cbb58b; " id="London" class="tabcontent">
		<div class="row">
		  <div class="col-lg-4">
		  <p style="text-align: center;"><a href="/masinstvo_obrada_metala"><img style="width:100%; height: 100%;" src="images/tab/masinstvo_obrada.png"></img></a></p>
		  <h3 style="text-align:center; color:white;">Mašinstvo i obrada metala</h3>
		  <hr>
		  <p style="text-align:center; color:white;">Kомпјутерско управљање.</p>
		  </div>
		  <div class="col-lg-4">
		  <p style="text-align: center;"><img style="width:100%; height: 100%;" src="images/tab/turizam_ugostiteljstvo.jpg"></img></p>
		  <h3 style="text-align:center; color:white;">Turizam i ugostiteljstvo</h3>
		  <hr>
		  <p style="text-align:center; color:white;">Кувари су одувек  били тражени у сваком месту.</p>
		  </div>
		</div>
		</div>

		<div  style="background-color:#cbb58b;  " id="Paris" class="tabcontent">
		   <div class="row">
		   <div class="col-lg-6" style="margin-top:40px;">
		   <div class="mapouter"><div class="gmap_canvas"><iframe width="450" height="300" id="gmap_canvas" 
		   src="https://maps.google.com/maps?q=prigrevacka%2072&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" 
		   scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://ytify.com">youtube mo3</a></div><style>
		   .mapouter{position:relative;text-align:right;height:300px;width:450px;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:450px;}</style></div> 
		   </div>
		   <div style="margin-top:20px;" class="col-lg-6">
		   <h3 style="text-align:center; color:white;">Дом ученика</h3>
		   <p style="text-align: justify; text-align-last: center;color:white;">Дом ученика има 20 соба, васпитача и интернет учионицу.</p>
		   <h3 style="text-align:center; color:white;">Школа</h3>
		   <p style="text-align: justify; text-align-last: center; color:white;">Школа има два спрата, библиотеку и одлично опремљене учионице.</p>
		   <h3 style="text-align:center; color:white;">Спортски објекти</h3>
		   <p style="text-align: justify; text-align-last: center; color:white;">Спортски објекти су терени за одбојку, кошарку, мали фудбал, велики терен за фудбал, затворена хала са свлачионицама.</p>
		   </div>
		   </div>
		</div>

		<div style="background-color: #cbb58b;  " id="Tokyo" class="tabcontent">
		<div class="row">
		  <div style="margin-top:25px;" class="col-lg-7">
		  <p style="text-align: justify; text-align-last: center; color:white;">Контактирајте нас на телефоне:<br>025/772-216 и 025/772-721<br>
			Или нам пошаљите факс на: <br> 025/772-744 <br> Или нам пошаљите поруку: <br> <a href="">Порука</a> </p> 
		  </div>
		  <div style="margin-top:20px;" class="col-lg-5">
		  <p style="text-align:center;"><img style="width:80%;" src="images/tab/kontakt.png"></img></p>
		  </div>
		</div>
		</div>
		<div style="background-color: #cbb58b;  " id="Tokyo2" class="tabcontent">
		<div class="row">
		  <div style="margin-top: 30px;" class="col-lg-6">
		  <h3 style="text-align:center; color:white;">Јавнe набавке</h3>
		  <p style="text-align: justify; text-align-last: center; color:white;">Школа је 17.02.2016. објавила у поступку   јавне  набавке мале вредности  – 1/2016 намирнице</p>
		  <h3 style="text-align:center; color:white;">Архива јавних набавки.</h3>
		  <p style="text-align: justify; text-align-last: center; color:white;">У архиви можете погледати све јавне набавке које је наша школа расписала до сада.</p>
		  </div>
		  <div style="margin-top: 10px;" class="col-lg-6">
		  <p style="text-align:center;"><img style="width:80%; height:80%;" src="images/tab/nabavke.jpg"></img></p>
		  </div>
		</div>
		</div>
	 </div>
    </section>
    <!--//END OUR COURSES -->
    <!--============================= EVENTS =============================-->

    <!--//END EVENTS -->
    <!--============================= DETAILED CHART =============================-->
    <div class="detailed_chart">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-8 chart_bottom">
                    <div class="chart-img">
                        <img src="images/chart-icon_1.png" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter">39</span> Profesora
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 chart_bottom chart_top">
                    <div class="chart-img">
                        <img src="images/chart-icon_2.png" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter">2600</span> Ucenika
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--//END DETAILED CHART -->
    <!--============================= OUR BLOG =============================-->
    <section id="blog" class="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Novosti</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="sajam_turizma" class="home_blog_link">
                        <div class="blog-img_box">
                            <img src="images/novosti/50.jpg" class="blog_display" alt="blog-img" style="width: 100%; height: 50vh;">
                            <div class="blogtitle">
                                <h3>5О МЕЂУНАРОДНИ САЈАМ ТУРИЗМА – НОВИ САД</h3>
                                <p>И ове школске године ученици образовног профила кувар-конобар имали су прилике да посете сајам туризма у Новом Сад.…</p>
                                <p>23.02.2019.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a href="#blog" class="home_blog_link">
                        <div style="height:70vh;" class="blog-img_box">
                            <img src="images/novosti/poseta.jpg" class="img-fluid blog_display" alt="blog-img">
                            <div class="blogtitle">
                                <h3>Посета школи UNGARNDEUTSCHES BILDUNGSZENTRUM BAJA</h3>
                                <p>Делегација школе имала је прилику да 16.02.2017. године посети престижну школу UNGARNDEUTSCHES BILDUNGSZENTRUM BAJA 
								у Мађарској у Баји. Делегацију школе чинили су директор Петар Поповић…</p>
                                <p>23.02.2019.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#blog" class="home_blog_link">
                        <div style="height:70vh;" class="blog-img_box">
                            <img src="images/novosti/tabla.jpg" class="img-fluid blog_display" alt="blog-img">
                            <div class="blogtitle">
                                <h3>Интерактивна табла</h3>
                                <p>Данас је у циљу побољшања наставног процеса, постављена мобилна интерактивна табла. У почетку ће се користити за наставу
								српског језика и књижевности,....</p>
                                <p>23.02.2019.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#blog" class="home_blog_link">
                        <div style="height:70vh;" class="blog-img_box">
                            <div class="blog-video">
                                <div class="blog-play_btn"> <img src="images/play-btn.png" alt="play-btn"> </div>
                                <img src="images/novosti/prva.jpg" class="img-fluid blog_display" alt="blog-img">
                            </div>
                            <!-- // end .blog-video -->
                            <div class="blogtitle">
                                <h3>ЗНАЧАЈ ПРВЕ ПОМОЋИ</h3>
                                <p>Ученици првих разреда присуствовали су предавању о значају прве помоћ.  Сви ученици који су заинтересовани за курс прве помоћи 
								треба да се јаве педагогици до 10.11.2016.године…</p>
                                <p>23.02.2019.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="/novosti" class="btn btn-default btn-courses">Pogledajte sve vesti</a>
                </div>
            </div>
        </div>
    </section>
    <!--//END OUR BLOG -->
    <!--============================= Instagram Feed =============================-->
    <!--<div id="instafeed"></div>-->
    <!--//END Instagram feed JS -->
    <!--============================= FOOTER =============================-->
    <footer>
        <div class="container">
            <!--<div class="row">-->
                <!--<div class="col-md-12">-->
                    <!--<div class="subscribe">-->
                        <!--<h3>Newsletter</h3>-->
                        <!--<form id="subscribeform" action="php/subscribe.php" method="post">-->
                            <!--<input class="signup_form" type="text" name="email" placeholder="Enter Your Email Address">-->
                            <!--<button type="submit" class="btn btn-warning" id="js-subscribe-btn">SUBSCRIBE</button>-->
                            <!--<div id="js-subscribe-result" data-success-msg="Success, Please check your email." data-error-msg="Oops! Something went wrong"></div>-->
                            <!--&lt;!&ndash; // end #js-subscribe-result &ndash;&gt;-->
                        <!--</form>-->
                    <!--</div>-->
                <!--</div>-->
            <!--</div>-->
            <div class="row">
                <div class="col-md-3">
                    <div class="foot-logo">
                        <a href="index.html">
                            <img src="images/footer-logo.png" class="img-fluid" alt="footer_logo">
                        </a>
                        <p>2019 © copyright
                            <br> Sva prava zadrzana.</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="sitemap">
                            <h3>Navigacija</h3>
                            <ul>
                                <li><a href="about.html">O Nama</a></li>
                                <li><a href="admission-form.html">Novosti</a></li>
                                <li><a href="admission.html">Galerija</a></li>
                                <li><a href="research.html">Kontakt</a></li>
                                <li><a href="contact.html">Javne nabavke</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <!--<div class="tweet_box">-->
                            <!--<h3>Tweets</h3>-->
                            <!--<div class="tweet-wrap">-->
                                <!--<div class="tweet"></div>-->
                                <!--&lt;!&ndash; // end .tweet &ndash;&gt;-->
                            <!--</div>-->
                        <!--</div>-->
                    </div>
                    <div class="col-md-3">
                        <div class="address">
                            <h3>Kontaktirajte nas</h3>
                            <p><span>Address: </span> Prigrevacka 72, Apatin</p>
                            <p>Email : info@tehnicksskolaapatin.com
                                <br> Telefon : 025/772-216</p>
                                <ul class="footer-social-icons">
                                    <li><a href="#"><i class="fa fa-facebook fa-fb" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin fa-in" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter fa-tw" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--//END FOOTER -->
			
            <script>
                var slideIndex = 1;
                showDivs(slideIndex);

                function plusDivs(n) {
                  showDivs(slideIndex += n);
                }

                function showDivs(n) {
                  var i;
                  var x = document.getElementsByClassName("mySlides");
                  if (n > x.length) {slideIndex = 1}
                  if (n < 1) {slideIndex = x.length}
                  for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";  
                  }
                  x[slideIndex-1].style.display = "block";  
                }

                $('.mySlides').on('dragstart', function(event) { event.preventDefault(); });
            </script>
		
			
			
			<script>
			function openCity(evt, cityName) {
			  var i, tabcontent, tablinks;
			  tabcontent = document.getElementsByClassName("tabcontent");
			  for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			  }
			  tablinks = document.getElementsByClassName("tablinks");
			  for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			  }
			  document.getElementById(cityName).style.display = "block";
			  evt.currentTarget.className += " active";
			}
			</script>
			
			
            <!-- jQuery, Bootstrap JS. -->
            <script src="js/jquery.min.js"></script>
            <script src="js/tether.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <!-- Plugins -->
            <script src="js/slick.min.js"></script>
            <script src="js/waypoints.min.js"></script>
            <script src="js/counterup.min.js"></script>
            <script src="js/instafeed.min.js"></script>
            <script src="js/owl.carousel.min.js"></script>
            <script src="js/validate.js"></script>
            <script src="js/tweetie.min.js"></script>
            <!-- Subscribe -->
            <script src="js/subscribe.js"></script>
            <!-- Script JS -->
            <script src="js/script.js"></script>
        </body>

        </html>
