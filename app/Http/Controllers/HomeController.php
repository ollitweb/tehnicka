<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomeSlider;

class HomeController extends Controller
{
    

	public function index(){
		$home_slider = HomeSlider::all();

		return view('index', compact('home_slider'));
	}

}
