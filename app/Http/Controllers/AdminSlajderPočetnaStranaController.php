<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminSlajderPočetnaStranaController extends CBController {


    public function cbInit()
    {
        $this->setTable("home_slider");
        $this->setPermalink("slajder_pocetna_strana");
        $this->setPageTitle("Slajder početna strana");

        $this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addImage("Image","image")->encrypt(true);
		$this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addWysiwyg("Content","content")->strLimit(150);
		

    }
}
