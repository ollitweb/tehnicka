<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminGalerijaController extends CBController {


    public function cbInit()
    {
        $this->setTable("gallery");
        $this->setPermalink("galerija");
        $this->setPageTitle("Galerija");

        $this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addImage("Image","image")->encrypt(true);
		

    }
}
