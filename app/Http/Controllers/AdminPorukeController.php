<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminPorukeController extends CBController {


    public function cbInit()
    {
        $this->setTable("messages");
        $this->setPermalink("poruke");
        $this->setPageTitle("Poruke");

        $this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addEmail("Email","email");
		$this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addWysiwyg("Content","content")->strLimit(150);
		

    }
}
