<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminKategorijaController extends CBController {


    public function cbInit()
    {
        $this->setTable("category");
        $this->setPermalink("kategorija");
        $this->setPageTitle("Kategorija");

        $this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Name","name")->strLimit(150)->maxLength(255);
		

    }
}
