<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminStraniceController extends CBController {


    public function cbInit()
    {
        $this->setTable("pages");
        $this->setPermalink("stranice");
        $this->setPageTitle("Stranice");

        $this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addText("Text","text")->strLimit(150)->maxLength(255);
		$this->addSelectTable("Category","category_id",["table"=>"category","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		

    }
}
