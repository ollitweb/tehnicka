<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $image
 * @property string $title
 * @property string $content
 */
class HomeSlider extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'home_slider';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['created_at', 'updated_at', 'image', 'title', 'content'];

}
